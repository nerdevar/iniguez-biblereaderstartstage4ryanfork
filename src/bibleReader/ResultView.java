package bibleReader;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import bibleReader.model.BibleReaderModel;

/**
 * The display panel for the Bible Reader.
 * 
 * @author cusack
 */
public class ResultView extends JPanel {
	// TODO Implement this class so it displays the results: Stage 5

	// TODO Add necessary Fields.

	BibleReaderModel model;
	JTextField stats;
	JScrollPane scroll;
	JEditorPane resultOutput;
	
	
	
	// You will probably want to use a JScrollPane and JTextArea or JEditorPane
	// for this, possibly in addition to some textfields, etc.
	// JEditorPanes can be set up to display simple HTML, which will come
	// in handy in future stages, so you might look into this.

	/**
	 * Construct a new ResultView and set its model to myModel. It needs to model to look things up.
	 * 
	 * @param myModel The model this view will access to get information.
	 */
	public ResultView(BibleReaderModel myModel) {
		// TODO Implement this method: Stage 5
		model = myModel;
		

	}

	// TODO add necessary methods. In particular, you need a method to set the results
	// so the view can update. You will likely add more methods as time goes on.

}

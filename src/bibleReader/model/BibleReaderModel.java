package bibleReader.model;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 */
public class BibleReaderModel implements MultiBibleModel {

	// ---------------------------------------------------------------------------
	// TODO Add more fields here: Stage 5
	// You need to store several Bible objects.
	// You may need to store other data as well.

	ArrayList<Bible> bibleList;
	String[] versionList;

	/**
	 * Default constructor. You probably need to instantiate objects and do other
	 * assorted things to set up the model.
	 */
	public BibleReaderModel() {
		bibleList = new ArrayList<Bible>();
	}

	@Override
	public String[] getVersions() {
		versionList = new String[bibleList.size()];
		int i = 0;
		while (i < bibleList.size()) {
			for (Bible b : bibleList) {
				versionList[i] = b.getVersion();
				i++;
			}
		}
		return versionList;
	}

	@Override
	public int getNumberOfVersions() {
		if (bibleList.isEmpty()) {
			return 0;
		} else {
			return bibleList.size();
		}
	}

	@Override
	public void addBible(Bible bible) {
		bibleList.add(bible);
	}

	@Override
	public Bible getBible(String version) {
		for (Bible b : bibleList) {
			if (b.getVersion().equals(version)) {
				return b;
			}
		}
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		for (Bible b : bibleList) {
			return b.getReferencesContaining(words);
		}
		return null;
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		for (Bible b : bibleList) {
			if (b.getVersion().equals(version)) {
				return b.getVerses(references);
			}
		}
		return null;
	}
	// ---------------------------------------------------------------------

	@Override
	public String getText(String version, Reference reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		// TODO Implement me: Stage 7
		return null;
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of delegate
	// methods in that they call a method on the Bible class to do most of the work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}
